<?php
namespace Jakobus;

use Contentomat\PsrAutoloader;
use Contentomat\Controller;
use Contentomat\CmtPage;
use Jakobus\Booking;

define('BOOKING_SUCCESS_PAGE_ID', 111);


class BookingsController extends Controller {


	/**
	 * @var Object
	 */
	protected $CmtPage;

	/**
	 * @var Jakobus\Booking
	 */
	protected $Booking;


	public function init () {
		$this->Booking = new Booking();
		$this->Booking->setLanguage ($this->pageLang);
		$this->CmtPage = new CmtPage();

		$this->templatesPath = $this->templatesPath . 'bookings/';
	}
	

	public function actionDefault() {

		$fields = $this->Booking->getFormFields(null, ['validate' => !empty($this->postvars)]);
		$this->parser->setMultipleParserVars($fields);
		if (!empty($this->postvars)) {

			if (empty($this->postvars['data-privacy-statement-accepted'])) {
				$this->parser->setParserVar('data-privacy-statement-not-accepted', true);
			}
			else {
				$data = $this->postvars;
				foreach($data as $key => $value) {
					$data[$key] = trim($value);
				}

				$data['booking_date'] = strftime('%Y-%m-%d %H:%M:%S');
				$check = $this->Booking->save($data);
				$validationErrors = $this->Booking->getValidationErrors();

				if (!$check) {
					$this->parser->setParserVar('saveFailed', true);
				}
				else {
					$this->Booking->notifyUser($data);
					$this->Booking->notifyAdmin($data);
					return $this->changeAction('success');
				}
			}
			$this->parser->setParserVar('booking_accomodation_value', $this->postvars['booking_accomodation']);
		}

		$this->content = $this->parser->parseTemplate($this->templatesPath . 'form.tpl');
	}

	public function actionSuccess () {
		$url = sprintf('%s%s', $this->CmtPage->makePageFilePath(BOOKING_SUCCESS_PAGE_ID), $this->CmtPage->makePageFileName(BOOKING_SUCCESS_PAGE_ID));
		header('Location: '.$url);
	}
}

$autoLoad = new PsrAutoloader ();
$autoLoad->addNamespace ('Jakobus', PATHTOWEBROOT . 'phpincludes/classes');
$content = (new BookingsController())->work();
?>
