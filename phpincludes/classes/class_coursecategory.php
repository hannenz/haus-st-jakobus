<?php
namespace Jakobus;

use Contentomat\CmtPage;
use Jakobus\Course;
use Jakobus\Event;

class CourseCategory extends Model {

	protected $CmtPage;

	protected $Course;

	protected $Event;



	protected $hasMany = [
		// [
		// 	'name' => 'Events',
		// 	'className' => 'Jakobus\Event',
		// 	'foreignKeyField' => 'event_course_category_id',
		// 	'foreignKey' => 'id',
		// 	'order' => 'course_pos ASC'
		// ]
		// [
		// 	'name' => 'Courses',
		// 	'className' => 'Jakobus\Course',
		// 	'foreignKeyField' => 'course_category_id',
		// 	'foreignKey' => 'id',
		// 	'order' => 'course_pos ASC'
		// ]
	];

	public function init () {
		$this->setTableName ('jakobus_course_categories');
		$this->CmtPage = new CmtPage();
		$this->Course = new Course();
		$this->Event = new Event();
	}

	public function afterRead($category) {

		$category['course_overview_url'] = sprintf('%s%s',
			$this->CmtPage->makePageFilePath($this->Course->getOverviewPageId(), $this->language),
			$this->CmtPage->makePageFileName($this->Course->getOverviewPageId(), $this->language)
		);
		// 2022-04-30: Refactoring to using only events
		$category['event_overview_url'] = sprintf('%s%s',
			$this->CmtPage->makePageFilePath($this->Event->getOverviewPageId(), $this->language),
			$this->CmtPage->makePageFileName($this->Event->getOverviewPageId(), $this->language)
		);

		$category['Events'] = $this->Event->findUpcomingByCategoryId($category['id']);

		// $query = sprintf('SELECT * FROM jakobus_events WHERE FIND_IN_SET(%u, event_course_category_id) ORDER BY event_begin ASC', $category['id']);
		// if ($this->db->query($query) == 0) {
		// 	$category['Events'] = $this->db->getAll();
		// }

		return $category;
	}
}
