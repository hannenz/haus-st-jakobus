<?php
namespace Jakobus;

use \Jakobus\Notification;


class Booking extends Model {

	protected $Notification;

	public function init () {

		$this->Notification = new Notification();
		$this->Notification->setTemplatesPath(PATHTOWEBROOT . "templates/bookings/notifications/");
		$this->Notification->setAdminNotificationRecipient("info@haus-st-jakobus.de");

		$this->setTableName('jakobus_bookings');
		$this->setupApplication();

		$this->setValidationRules([
			'booking_salutation' => '/Herr|Frau/',
			'booking_firstname' => '/^.+$/',
			'booking_lastname' => '/^.+$/',
			'booking_street_address' => '/^.+$/',
			// 'booking_country' => '/^.+$/',
			'booking_zip' => '/^.+$/',
			'booking_city' => [
				'not-empty' => '/^.+$/'
			],
			'booking_email' => '/^.+@.+\..{2,3}$/',
			'booking_phone' => '/^.+$/',
			'booking_arrival' => 'validateArrival',
			'booking_departure' => 'validateDeparture',
			'booking_adults' => 'validateAmount',
			'booking_children' => 'validateAmount',
			'booking_infants' => 'validateAmount',
			'booking_accomodation' => '/^.+$/',
			// 'booking_dinner' =>     '/^.*$/',
			// 'booking_breakfast' =>  '/^.*$/',
			// 'booking_vegetarian' => '/^.*$/'
		]);
	}


	protected function validateAmount($value) {
		return (intval($value) >= 0);
	}

	protected function validateArrival($value, $data = null) {
		$nowTimestamp = time();
		$arrivalTimestamp = strtotime($value . ' 23:59:59');
		if ($arrivalTimestamp === false) {
			return false;
		}
		return ($arrivalTimestamp >= $nowTimestamp);
	}



	protected function validateDeparture($value, $data = null) {
		// $data seems not to be working (it should!), so I use $_POST
		$arrivalTimestamp = strtotime($_POST['booking_arrival']);
		$departureTimestamp = strtotime($value);
		if ($arrivalTimestamp === false || $departureTimestamp === false) {
			return false;
		}
		$check = ($departureTimestamp > $arrivalTimestamp && $departureTimestamp >= time());
		return $check;
	}



	public function notifyUser($data) {
		$recipient = $data['booking_email'];

		return $this->Notification->notify(
			$recipient,
			'[Haus St. Jakobus] Buchung Pilgerbett',
			'notify_user',
			$data
		);
	}



	public function notifyAdmin($data) {

		return $this->Notification->notifyAdmin(
			'[Haus St. Jakobus] Buchung Pilgerbett',
			'notify_admin',
			$data
		);
	}



	public function beforeSave($data) {


	}
}
