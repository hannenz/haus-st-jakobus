<?php
namespace Jakobus;

use Contentomat\CmtPage;
use Jakobus\Model;
use Jakobus\Notification;
use \Exception;

/**
 * Handle donations
 *
 * @class Donation
 * @package jakobus
 * @author Johannes Braun <johannes.braun@agentur-halma.de>
 */
class Membership extends Model {

	/**
	 * @var string
	 */
	protected $adminNotificationRecipient = 'info@haus-st-jakobus.de';

	/**
	 * @var int
	 */
	protected $successPageId = 109;

	/**
	 * @var \Jakobus\Notiifcation
	 */
	protected $Notification;

	/**
	 * @var \Contentomat\CmtPage
	 */
	protected $CmtPage;

	/**
	 * @var string
	 */
	protected $errorMessage = '';


	public function init() {

		$this->setTableName('jakobus_memberships');
		$this->Notification = new Notification();
		$this->Notification->setTemplatesPath(PATHTOWEBROOT . "templates/memberships/notifications/");
		$this->CmtPage = new CmtPage();

		$this->setValidationRules([
			'membership_salutation' => '/Herr|Frau|Firma/',
			'membership_firstname' => '/^.+$/',
			'membership_lastname' => '/^.+$/',
			'membership_street_address' => '/^.+$/',
			'membership_zip' => '/^.+$/',
			'membership_city' => [
				'not-empty' => '/^.+$/'
			],
			'membership_email' => '/^.+@.+\..{2,3}$/',
			'membership_phone' => '/^.+$/',
			'membership_account_holder' => '/^.+$/',
			'membership_bank' => '/^.+$/',
			'membership_iban' => 'validateIban',
			'membership_bic' => '/([a-zA-Z]{4})([a-zA-Z]{2})(([2-9a-zA-Z]{1})([0-9a-np-zA-NP-Z]{1}))((([0-9a-wy-zA-WY-Z]{1})([0-9a-zA-Z]{2}))|([xX]{3})|)/',
			'membership_allow_direct_debit' => '/1/',
			'membership_amount' => 'validateAmount',
			// 'membership_birthday' => '/^\d{4}\-\d{2}\-\d{2}$/'
		]);
	}



	/**
	 * Validate IBAN
	 * @see https://github.com/cakephp/cakephp/blob/e9b3d06c1a48ccdeeac4d44286428a38368eaae9/src/Validation/Validation.php#L1685
	 */
	protected static function validateIban($check): bool {
		$check = preg_replace('/\s+/', '', $check);
		if (
			!is_string($check) ||
			!preg_match('/^[A-Z]{2}[0-9]{2}[A-Z0-9]{1,30}$/', $check)
		) {
			return false;
		}

		$country = substr($check, 0, 2);
		$checkInt = intval(substr($check, 2, 2));
		$account = substr($check, 4);
		$search = range('A', 'Z');
		$replace = [];
		foreach (range(10, 35) as $tmp) {
			$replace[] = strval($tmp);
		}
		$numStr = str_replace($search, $replace, $account . $country . '00');
		$checksum = intval(substr($numStr, 0, 1));
		$numStrLength = strlen($numStr);
		for ($pos = 1; $pos < $numStrLength; $pos++) {
			$checksum *= 10;
			$checksum += intval(substr($numStr, $pos, 1));
			$checksum %= 97;
		}

		return $checkInt === 98 - $checksum;
	}


	public function validateAmount($amount) : bool{
		$uAmount = intval($amount);
		return ($uAmount == 70 || $uAmount >= 100);
	}


	/**
	 * Send a notification to website admin
	 *
	 * @throws Exception
	 * @return void
	 */
	public function notifyAdmin($data) {
		$data['membershipDateTimeFmt'] = strftime('%d.%m.%Y %H:%M:%S');

		return $this->Notification->notify(
			$this->adminNotificationRecipient,
			'Ein Antrag auf Mitgliedschaft im Förderverin ist eingegangen',
			'notify_admin',
			$data
		);
	}



	public function notifyUser($data) {
		$recipient = $data['membership_email'];

		return $this->Notification->notify(
			$recipient,
			'[Haus St. Jakobus] Ihr Antrag auf Mitgliedschaft im Förderverein',
			'notify_user',
			$data
		);
	}



	/**
	 * Getter for errorMessage
	 *
	 * @return string
	 */
	public function getErrorMessage() {
	    return $this->errorMessage;
	}
}
