<?php
namespace Jakobus;

use Contentomat\PsrAutoloader;
use Contentomat\Controller;
use Contentomat\CmtPage;
use Jakobus\Membership;
use Contentomat\Logger;

use Exception;

define('MEMBERSHIP_SUCCESS_PAGE_ID', 109);

class MembershipsController extends Controller {

	/**
	 * @var Jakobus\Membership
	 */
	protected $Membership;

	/**
	 * @var Contentomat\CmtPage
	 */
	protected $CmtPage;



	public function init() {
		$this->Membership = new Membership();
		$this->Membership->setLanguage($this->pageLang);
		$this->CmtPage = new CmtPage();
		$this->templatesPath = $this->templatesPath . 'memberships/';
	}



	public function actionDefault() {

		$fields = $this->Membership->getFormFields(null, ['validate' => !empty($this->postvars)]);
		$this->parser->setMultipleParserVars($fields);

		if (isset($this->postvars['action']) && $this->postvars['action'] == 'default') {

			$proceed = true;
			if (empty($this->postvars['data_privacy_statement_accepted'])) {
				$this->parser->setParserVar('data-privacy-statement-not-accepted', true);
			}
			else {
				$this->parser->setParserVar('data_privacy_statement_accepted', true);
			}
			if (empty($this->postvars['membership_allow_direct_debit'])) {
				$this->parser->setParserVar('membership-allow-direct-debit-not-accepted', true);
			}

			if ($proceed) {
				$data = $this->postvars;
				foreach($data as $key => $value) {
					$data[$key] = trim($value);
				}
				if ($data['membership_amount'] == 'custom') {
					$data['membership_amount'] = (int)trim($data['membership_amount_custom']);
				}
				if (empty($data['membership_amount']) || ($data['membership_amount'] != 70 && $data['membership_amount'] < 100)) {
					$this->parser->setParserVar('membership_amount_invalid', true);
				}
				$this->parser->setParserVar('membership_amount_custom', $data['membership_amount_custom']);

				$data['membership_date'] = strftime('%Y-%m-%d %H:%I:%S');

				if (!$this->Membership->save($data)) {
					$this->parser->setParserVar('saveFailed', true);
				}
				else {
					$this->Membership->notifyUser($data);
					$this->Membership->notifyAdmin($data);
					return $this->changeAction('success');
				}
			}
		}

		$this->parser->setParserVar('membership_amount', '');
		$this->content = $this->parser->parseTemplate($this->templatesPath . 'form.tpl');
	}



	public function actionSuccess() {
		$url = sprintf('%s%s', $this->CmtPage->makePageFilePath(MEMBERSHIP_SUCCESS_PAGE_ID), $this->CmtPage->makePageFileName(MEMBERSHIP_SUCCESS_PAGE_ID));
		header('Location: '.$url);
	}



	public function actionTestMail() {
		$this->Membership->notifyAdmin('me@hannenz.de', '', [
			'membership_salutation' => 'Herr',
			'membership_firstname' => 'Johannes',
			'membership_lastname' => 'Braun',
			'membership_street_address' => 'Gartenstraße 13',
			'membership_zip' => '89614',
			'membership_city' => 'Öpfingen',
			'membership_email' => 'hannenz@posteo.de',
			'membership_account_holder' => 'Johannes Braun',
			'membership_bank' => 'ING',
			'membership_iban' => 'DE88 5601 ',
			'membership_bic' => 'INGDDEFFXX',
		]);
	}
}

$autoload = new PsrAutoloader();
$autoload->addNamespace('Contentomat', PATHTOADMIN.'classes');
$autoload->addNamespace('Jakobus', PATHTOWEBROOT.'phpincludes/classes');
$content = (new MembershipsController())->work();
?>
