<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitb30cbcaf7dc8506ce30d8a7f1efa0e83
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Symfony\\Component\\Finder\\' => 25,
        ),
        'H' => 
        array (
            'Hannenz\\HausStJakobus\\' => 22,
        ),
        'G' => 
        array (
            'Gregwar\\' => 8,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Symfony\\Component\\Finder\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/finder',
        ),
        'Hannenz\\HausStJakobus\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
        'Gregwar\\' => 
        array (
            0 => __DIR__ . '/..' . '/gregwar/captcha/src/Gregwar',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitb30cbcaf7dc8506ce30d8a7f1efa0e83::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitb30cbcaf7dc8506ce30d8a7f1efa0e83::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitb30cbcaf7dc8506ce30d8a7f1efa0e83::$classMap;

        }, null, ClassLoader::class);
    }
}
