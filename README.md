2024-12-01

To run the website as deployed (php 7):

```
git checkout production
docker composer -f docker/docker-compose.yml up -d
```

Then the website is served at `https://localhost:7373`

---


Website https://www.haus-st-jakobus.de

2022-04-30: Migrated this Repo from Github to Codeberg.


Todos:

- Pilgerausweis Bestellbestätigiungs Screen bei Bargeld: Betrag wird
nicht angezeigt

- Mitgliedschaft Förderverein: Betragsauswahl wird bei POST request
nicht wieder gesetzt falls Fomrular nicht verarbeitet werden konnte.
