Dev LAMP in docker containers for PHP 7 Environment.
Start it up with

```
docker compose up -d
```

Then access the website at port 7373

```
https://haus-st-jakobus.localhost:7373
```
