FROM php:7.1-apache-buster
RUN apt-get update
RUN apt-get install --assume-yes --no-install-recommends libfreetype6-dev libpng-dev libjpeg62-turbo-dev locales-all locales vim 

# Mailpit
RUN sh -c 'curl -Lk https://github.com/axllent/mailpit/releases/download/v1.13.1/mailpit-linux-amd64.tar.gz | tar -C /usr/local/bin -xzf -'

RUN a2enmod rewrite vhost_alias ssl headers
COPY ./config/apache2/conf-available/httpd-vhosts.conf /etc/apache2/conf-available
RUN a2enconf httpd-vhosts.conf

RUN docker-php-ext-install -j$(nproc) mysqli mbstring gd exif 

RUN docker-php-ext-configure gd \
	--with-gd \
	--with-jpeg-dir \
	--with-png-dir \
	--with-zlib-dir \
	--with-freetype-dir

# A little bit of comfort :-)
COPY ./config/minivimrc /root/.vimrc
RUN echo 'set -o vi' >> /root/.bashrc

# And clean up the image
RUN rm -rf /var/lib/apt/lists/*
