<body style="margin: 0; padding: 0; height: 100%;">
<table style="width: 100%; height: 100%; border-collapse:collapse; font-size: 16px; line-height: 20px; font-family: serif;" cellpadding="0" cellspacing="0"><tr><td>
	<table style="width: 100%; height: 100%; background-color: #2e3436;" cellpadding="0" cellpadding="0" cellspacing="0">
		<tr style="background-color: #a71c21">
			<td>&nbsp;</td>
			<td style="width: 700px; background-color: #a71c21; height: 100px;">
				<div style="margin: 20px">
					<a href="https://www.haus-st-jakobus.de" style="color:#fff; text-decoration:none">
						<img src="/dist/img/logo.png" alt="Jakobsmuschel" width="48" height="48" /><br>
						<span style="color: #fff">Haus St. Jakobus</span>
					</a>
				</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td style="background-color: #efd2d3">&nbsp;</td>
			<td style="height: 16px; background-color: #efd2d3; color: #a71c21; font-size: 13px; font-weight: bold; font-style: italic; text-align: left">
				<div style="margin: 10px 20px ">
					&bdquo;Kommt und ruht ein wenig aus &hellip;&ldquo;
				</div>
			</td>
			<td style="background-color: #efd2d3">&nbsp;</td>
		</tr>
		<tr style="background-color:  #f8f8f8;">
			<td style="background-color:  #f8f8f8;">&nbsp;</td>
			<td style="height: 500px; width: 700px; vertical-align: top; background-color: #f8f8f8;">
				<div style="margin: 20px; font-family: serif; font-size: 16px; line-height: 20px; color: #404040; ">
				{VAR:mailContent}
				</div>
			</td>
			<td style="background-color:  #f8f8f8;">&nbsp;</td>
		</tr>
		<tr>
			<td style="background-color:  #b0b0b0;">&nbsp;</td>
			<td style="background-color: #b0b0b0">
				<div style="margin: 5px 20px; font-family: serif; font-size: 14px; line-height: 16px; color: #404040; ">
					Im Web: <a style="color: #2e3436" href="https://www.haus-st-jakobus.de">https://wwww.haus-st-jakobus.de</a>
				</div>
				<div style="margin: 5px 20px; font-family: serif; font-size: 14px; line-height: 16px; color: #404040; ">
					<a style="color: #404040; text-decoration: underline" href="https://www.haus-st-jakobus.de{PAGEURL:24}">Pilgerausweise</a> &middot; 
					<a style="color: #404040; text-decoration: underline" href="https://www.haus-st-jakobus.de{PAGEURL:26}">Pilgerherberge</a> &middot; 
					<a style="color: #404040; text-decoration: underline" href="https://www.haus-st-jakobus.de{PAGEURL:26}">Pilgerwege</a> &middot; 
					<a style="color: #404040; text-decoration: underline" href="https://www.haus-st-jakobus.de{PAGEURL:107}">Veranstaltungen</a> &middot; 
					<a style="color: #404040; text-decoration: underline" href="https://www.haus-st-jakobus.de{PAGEURL:61}">Förderverein</a>
				</div>
			</td>
			<td style="background-color:  #b0b0b0;">&nbsp;</td>
		</tr>
		<tr style="background-color: #2e3436">
			<td>&nbsp;</td>
			<td style="width: 700px; background-color: #2e3436; vertical-align: top">
				<div style="margin: 20px; font-style:italic">
					<p style="color: #b9b9b9; font-size: 14px;">
						Cursillo-Haus St. Jakobus<br>
						Kapellenberg 58-60<br>
						D–89610 Oberdischingen
					</p>
					<p style="color: #b9b9b9; font-style: italic; font-size: 14px;">
						07305 – 919 575<br>
						<a style="color: #b9b9b9; text-decoration: underline" href="mailto:info@haus-st-jakobus.de">info@haus-st-jakobus.de</a>
					</p>
				</div>
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</td></tr></table>
</body>
