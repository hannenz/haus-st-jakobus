<h3 class="headline headline--section-title">{VAR:course_category_name}</h3>

{IF({COUNT:events} == 0)}
	<p>
		Zur Zeit keine Veranstaltungen dieser Art.
	</p>
{ELSE}
	<!-- <p>{COUNT:events} Veranstaltungen</p> -->
	<div class="events events--by-category">
		{LOOP VAR(events)}
			{INCLUDE:PATHTOWEBROOT."templates/events/teaser.tpl"}
		{ENDLOOP VAR}
	</div>
{ENDIF}
