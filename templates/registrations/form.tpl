<form id="registration" action="" method="post" novalidate>

	{IF({ISSET:saveFailed:VAR})}
		<div class="error message">Bitte prüfen Sie Ihre Eingabe und versuchen Sie es noch einmal!</div>
	{ENDIF}
	{IF({ISSET:data-privacy-statement-not-accepted})}
		<p class="error message message--error">Sie müssen unserer Datenschutzerklärung zustimmen, damit wir Ihre Anmeldung verarbeiten können.</p>
	{ENDIF}

	<fieldset class="fieldset">
		<legend>Anmeldung zur Veranstaltung</legend>
		<div>
			<strong>Veranstaltung: {VAR:event_title}</strong><br>
			<em>Termin: {VAR:event_date_fmt}</em>
			<input type="hidden" readonly name="registration_event_id" value="{VAR:id}" />
		</div>
		<div>
	</fieldset>

	<fieldset class="fieldset">
		<legend>Persönliche Angaben</legend>

		<div class="grid-x grid-padding-x">
			<div class="cell medium-3">
				{VAR:registration_salutation}
			</div>
			<div class="cell medium-4">
				{VAR:registration_firstname}
			</div>
			<div class="cell medium-5">
				{VAR:registration_lastname}
			</div>
		</div>

		<div class="grid-x grid-padding-x">
			<div class="cell">
				{VAR:registration_street_address}
			</div>
		</div>

		<div class="grid-x grid-padding-x">
			<div class="cell medium-2">
				{VAR:registration_zip}
			</div>
			<div class="cell medium-5">
				{VAR:registration_city}
			</div>
			<div class="cell medium-5">
				{VAR:registration_country}
			</div>
		</div>


		<div class="grid-x grid-padding-x">
			<div class="cell medium-6">
				{VAR:registration_email}
			</div>
			<div class="cell medium-6">
				{VAR:registration_phone}
			</div>
		</div>

		<div class="grid-x grid-padding-x">
			<div class="cell medium-6">
				{VAR:registration_birthday}
			</div>
			<div class="cell medium-6">
				{VAR:registration_diocese}
			</div>
		</div>
		<!-- <div class="grid-x grid-padding-x"> -->
		<!-- 	<div class="cell medium-6"> -->
		<!-- 		{VAR:registration_is_member} -->
		<!-- 	</div> -->
		<!-- </div> -->

		<div class="grid-x grid-padding-x">
			<div class="cell medium-6">
				<div class="info">
					Die mit einem <span>*</span> gekennzeichneten Felder müssen ausgefüllt werden.
				</div>
			</div>
		</div>
	</fieldset>

	<fieldset class="fieldset">
		<legend>Übernachtung</legend>

		<div class="grid-x grid-padding-x">
			<div class="cell medium-12">
				<div class="form-field--radio">
					<p><label>Ich wünsche</label></p>
					<div class="stack">
						<div class="radio-wrapper">
							<input 
								type="radio"
								name="registration_accomodation"
								id="registration-accomodation--double-room"
								value="Mehrfachzimmer"
								{IF('{VAR:registration_accomodation_value}' == 'Mehrfachzimmer')} checked {ENDIF}
							/>
							<label for="registration-accomodation--double-room">Übernachtung im Doppel- oder Dreibettzimmer (15 Euro pro Nacht und Person günstiger als Einzelzimmer)</label>
						</div>
						<div class="radio-wrapper">
							<input
								type="radio"
								name="registration_accomodation"
								id="registration-accomodation--single-room"
								value="Einzelzimmer"
								{IF('{VAR:registration_accomodation_value}' == 'Einzelzimmer')}checked{ENDIF}
							/>
							<label for=registration-accomodation--single-room />Übernachtung im Einzelzimmer</label>
						</div>
						<div class="radio-wrapper">
							<input
								type="radio"
								name="registration_accomodation"
								id="registration-accomodation--single-room-preferred"
								value="Einzelzimmer-bevorzugt" 
								{IF('{VAR:registration_accomodation_value}' == 'Einzelzimmer-bevorzugt')}checked{ENDIF}
							/>
							<label for=registration-accomodation--single-room-preferred>Übernachtung im Einzelzimmer bevorzugt, wenn kein Einzelzimmer frei ist, bin ich auch bereit im Doppelzimmer zu übernachten</label>
						</div>
						<div class="radio-wrapper">
							<input
								type="radio"
								name="registration_accomodation"
								id="registration-accomodation--none"
								value="Keine"
								{IF('{VAR:registration_accomodation_value}' == 'Keine')}checked{ENDIF}
							/>
							<label for=registration-accomodation--none>Keine Übernachtung</label>
						</div>
					</div>
				</div>
				<div class="form-field form-field--flag">
					{VAR:registration_vegetarian}
				</div>
			</div>
		</div>

	</fieldset>

	<fieldset class="fieldset">
		<legend>Anmerkungen</legend>
		<div class="form-field form-field--textarea">
			{VAR:registration_remarks}
		</div>
	</fieldset>

	<fieldset class="fieldset">
		<legend>Datenschutz</legend>
		<div class="form-field form-field--flag">
			<input type="checkbox" name="data-privacy-statement-accepted" id="data-privacy-statement-accepted" />
			<label for="data-privacy-statement-accepted">Ich habe die <a href="{PAGEURL:58}" target="_blank">Datenschutzerklärung</a> gelesen und erkläre mich hiermit einverstanden.</label>
		</div>
	</fieldset>

	<div class="action-area">
		<button type="submit" class="button">Anmelden</button>
		<a class="" href="{VAR:course_detail_url}">Zurück</a>
	</div>

</form>
