<p>Sehr geehrte{IF("{VAR:registration_salutation}" == "Herr")}r{ENDIF} {VAR:registration_salutation} {VAR:registration_lastname}</p>
<p>herzlichen Dank, Ihre Anmeldedaten sind bei uns eingegangen.</p>
<p>Solange Sie keine andere Antwort bekommen, sind Sie zu der Veranstaltung zugelassen.</p>
<p>Wir melden uns einige Tage vor der Veranstaltung bei Ihnen mit weiteren Informationen.</p>
<p style="font-style: italic; font-weight: bold; margin-top: 12px; margin-bottom: 12px">{VAR:event_title} am {VAR:event_date_fmt}</p>
<p>
	{VAR:complimentary_close:nl2br}
</p>
