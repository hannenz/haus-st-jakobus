Eine Anmeldung ist eingegangen

Name: 			{VAR:registration_salutation} {VAR:registration_firstname} {VAR:registration_lastname}
Anschrift: 		{VAR:registration_street_address}
				{VAR:registration_zip} {VAR:registration_city}
				{VAR:registration_country}
E-Mail: 		{VAR:registration_email}
Telefon: 		{VAR:registration_phone}
Diözese/Bistum:	{VAR:registration_diocese}
Übernachtung:   {VAR:registration_accomodation_value}
Vegetarisch:    {IF("{VAR:registration_vegetarian}" == "1"})Ja{ELSE}Nein{ENDIF}
Anmerkungen:	{VAR:registration_remarks}

Veranstaltung: {VAR:event_title} am {VAR:event_date_fmt}

--

