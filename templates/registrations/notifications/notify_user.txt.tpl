Sehr geehrte{IF("{VAR:registration_salutation}" == "Herr")}r{ENDIF} {VAR:registration_salutation} {VAR:registration_lastname}


herzlichen Dank, Ihre Anmeldedaten sind bei uns eingegangen.

Solange Sie keine andere Antwort bekommen, sind Sie zu der Veranstaltung zugelassen.

Wir melden uns einige Tage vor der Veranstaltung bei Ihnen mit weiteren Informationen.

{VAR:event_title} am {VAR:event_date_fmt}


{VAR:complimentary_close:nl2br}
