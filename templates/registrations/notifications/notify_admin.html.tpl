<p>Eine Kurs-Anmeldung ist eingegangen mit den folgenden Daten:</p>

<table>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Name</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:registration_salutation} {VAR:registration_firstname} {VAR:registration_lastname}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Anschrift</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">
			{VAR:registration_street_address}<br>
			{VAR:registration_zip} {VAR:registration_city}<br>
			{VAR:registration_country}
		</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">E-Mail</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:registration_email}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Telefonnummer</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:registration_phone}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Geburtstag</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:registration_birthday_fmt}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Diözese/Bistum</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:registration_diocese}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Übernachtung</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:registration_accomodation}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Vegetarisch</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{IF("{VAR:registration_vegetarian}" == "1")}Ja{ELSE}Nein{ENDIF}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Anmerkungen</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:registration_remarks}</td>
	</tr>
</table>

<table style="margin-top: 64px 32px 4px 0">
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Veranstaltung</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:event_title} am {VAR:event_date_fmt}</td>
	</tr>
</table>

