{EVAL}
use Contentomat\PsrAutoloader;
use Jakobus\CorporateData;

$autoLoader = new PsrAutoloader();
$autoLoader->addNamespace('Jakobus', PATHTOWEBROOT . "phpincludes/classes");

try {
	$CorporateData = new CorporateData();
	$cdOrgName = $CorporateData->getField('address_organization');
	$cdStreetAddress = $CorporateData->getField('address_street_address');
	$cdZip = $CorporateData->getField('address_zip');
	$cdCity = $CorporateData->getField('address_city');
	$cdCountry = $CorporateData->getField('address_country');
	$cdClaim = $CorporateData->getField('claim');
	$cdFooterAddressBlock = $CorporateData->getField('footer_address_block');
	$cdFooterBankAccountBlock = $CorporateData->getField('footer_bank_account');
}
catch (\Exception $e) {
	die ($e->getMessage());
}
{ENDEVAL}


 <figure class="map">
	 <button class="button" id="map-trigger-btn"><span>Karte groß</span><span hidden>Karte klein</span></button>
	 <div id="map"></div>
 </figure>

<footer class="main-footer">
	<div class="main-footer__inner inner-bound">
		<div class="footer-column">
			<address>
				<p>
					{USERVAR:cdFooterAddressBlock:nl2br}
				</p>
				<p>
					{USERVAR:cdFooterBankAccountBlock:nl2br}
				</p>
			</address>
		</div>
			<!--
	   		jobr@2024-02-18: Disabled on client request
	   		-------------------------------------------
			->
			<div class="footer-column">
			<!-- <nav> -->
			<!-- 	<ul> -->
			<!-- 		<li><a href="{PAGEURL:25}">Der Weg</a></li> -->
			<!-- 		<li><a href="{PAGEURL:92}">Das Haus</a></li> -->
			<!-- 		<li><a href="{PAGEURL:61}">Förderverein</a></li> -->
			<!-- 		<li><a href="{PAGEURL:24}">Pilgerausweise</a></li> -->
			<!-- 		<li><a href="{PAGEURL:39}">Programm</a></li> -->
			<!-- 	</ul> -->
			<!-- </nav> -->
		<!-- </div> -->
		<div class="footer-column">
			<blockquote>
				{USERVAR:cdClaim:nl2br}
			</blockquote>
			<nav class="legal-nav">
				{LOOP NAVIGATION(40)}
					<a href="{NAVIGATION:link}">{NAVIGATION:title}</a>
				{ENDLOOP NAVIGATION}
			</nav>
		</div>
	</div>
</footer>
