<h3>Eine Spende ist eingegangen:</h3>

<table>
	<tr style="border-top: 1px solid #e0e0e0"> 
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Betrag</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:amountFmt} &euro;</td> 
	</tr>
	<tr style="border-top: 1px solid #e0e0e0"> 
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Bezahlmethode</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:payment_method}</td> 
	</tr>
</table>

<h3>Persönliche Daten</h3>

<table>
	<tr style="border-top: 1px solid #e0e0e0">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Anrede</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:donation_salutation}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Vorname</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:donation_firstname}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Nachname</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:donation_lastname}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">PLZ</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:donation_zip}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Stadt</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:donation_city}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Land</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:donation_country}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Datum / Uhrzeit</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:donationDateTimeFmt}</td>
	</tr>
</table>
