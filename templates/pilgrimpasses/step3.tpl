{IF({ISSET:saveFailed:VAR})}
	<p class="error message">Bitte prüfen Sie Ihre Eingabe und versuchen Sie es noch einmal!</p>
{ENDIF}

<h2 class="headline">Pilgerausweise bestellen </h2>

<form id="pilgrimpass-form" name="step3" action="" method="post">

	<fieldset class="fieldset">
		<legend>Mitteilung</legend>
		<div class="form-field form-field--text-area">
			<label for="order_message">Mitteilung</label>
			<textarea rows="4" name="order_message" id="order_message" rows="10" placeholder="Hier haben Sie noch die Möglichkeit, Ihrer Bestellung eine Bemerkung hinzuzufügen">{VAR:order_message}</textarea>
		</div>
	</fieldset>

	<input type="hidden" name="step" value="3" />
	<div class="action-area"> 
		<button class="button" type="submit" name="action" value="step3">Weiter</button>
		<button class="button" name="action" value="back">Zurück</button>
		<a class="pilgrimpass-abort" data-confirm-text="Sind Sie sicher, dass Sie die Bestellung abbrechen möchten?" href="{PAGEURL}?action=abort">Abbrechen</a>
	</div>
</form>

<script>
var select = document.getElementById('order_payment_method');
select.addEventListener('change', onChange);
function onChange() {
	document.getElementById('bank-details').hidden = (select.value != 'ueberweisung');
}
onChange();
</script>
