<p> Vielen Dank, {VAR:order_delivery_address_salutation} {VAR:order_delivery_address_lastname}<br>
Ihre Bestellung ist bei uns eingegangen.</p>

<h2 class="headline headline">Der Pilgerausweis ist wertvoll, aber kostenlos</h2>
<p>Durch <strong>Ihr Spende</strong> unterstützen Sie unsere Arbeit.</p>


<ul class="donations">
	<li>
		<h3 class="headline headline--section-title">PayPal</h3>
		<div>
		<p>Klicken Sie auf den unten stehenden Button, um mit PayPal zu spenden</p>
		<form action="https://www.paypal.com/donate" method="post" target="_blank">
			<input type="hidden" name="hosted_button_id" value="LH4RB9V4UY6NW" />
			<input type="image" src="https://www.paypalobjects.com/de_DE/DE/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
			<img alt="" border="0" src="https://www.paypal.com/en_DE/i/scr/pixel.gif" width="1" height="1" />
		</form>
		</div>
	</li>

	<li>
		<h3 class="headline headline--section-title">Banküberweisung</h3>
		<div>
		<p>Bitte überweisen Sie den gewünschten Betrag auf folgendes Konto:</p>
		<p>
			{VAR:corporate_data_bank_account_name}<br>
			IBAN: {VAR:corporate_data_bank_account_iban}<br>
			BIC: {VAR:corporate_data_bank_account_bic}<br>
			Verwendungszweck: <span>&laquo;Bestellnr {VAR:orderId}&raquo;</span>
		</p>
		</div>
	</li>

	<li>
		<h3 class="headline headline--section-title">Bargeld</h3>
		<div>
		<p>Bitte senden Sie den gewünschten Betrag per Post an diese Adresse</p>
		<p>
			{VAR:corporate_data_address_organization}<br>
			{VAR:corporate_data_address_street_address}<br>
			{VAR:corporate_data_address_country}&ndash;{VAR:corporate_data_address_zip} {VAR:corporate_data_address_city}<br>
		</p>
		<p>
			Bitte vermerken Sie in Ihrem Schreiben die <em>Bestellnr. {VAR:orderId}</em>
		</p>
		</div>
	</li>
</ul>
		<p><button class="button" onclick="window.print()">Diese Seite drucken</button></p>
