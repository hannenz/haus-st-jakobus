<form id="membership-form" action="#membership-form" method="post" novalidate>
	<input type="hidden" name="action" value="default">

	{IF({ISSET:saveFailed:VAR})}
		<p class="error message">Bitte prüfen Sie Ihre Eingabe und versuchen Sie es noch einmal!</p>
	{ENDIF}
	{IF({ISSET:membership_amount_invalid})}
		<p class="error message">Bitte Betrag wählen oder einen Betrag eingeben, der höher ist als 100,00 &euro;</p>
	{ENDIF}
	{IF({ISSET:data-privacy-statement-not-accepted})}
		<p class="error message message--error">Sie müssen unserer Datenschutzerklärung zustimmen, damit wir Ihren Antrag verarbeiten können.</p>
	{ENDIF}
	{IF({ISSET:membership-allow-direct-debit-not-accepted})}
		<p class="error message message--error">Sie müssen dem Lastschrifteinzug zustimmen, damit wir Ihren Antrag verarbeiten können.</p>
	{ENDIF}

	<fieldset class="fieldset">
		<legend>Persönliche Angaben und Jahresbeitrag</legend>

		<div class="grid-x grid-padding-x">
			<div class="cell">
				<p class="info">Hiermit erkläre ich meinen Beitritt zum Förderverein „Cursillo-Haus St. Jakobus – Schwäbische Jakobusgesellschaft - Geistliche Bildungs- und Begegnungsstätte e. V.“:</p>
			</div>
		</div>

			
		<div class="grid-x grid-padding-x">
			<div class="cell medium-3">
				{VAR:membership_salutation}
			</div>
			<div class="cell medium-4">
				{VAR:membership_firstname}
			</div>
			<div class="cell medium-5">
				{VAR:membership_lastname}
			</div>
		</div>

		<div class="grid-x grid-padding-x">
			<div class="cell">
				{VAR:membership_street_address}
			</div>
		</div>

		<div class="grid-x grid-padding-x">
			<div class="cell medium-2">
				{VAR:membership_zip}
			</div>
			<div class="cell medium-5">
				{VAR:membership_city}
			</div>
			<div class="cell medium-5">
				{VAR:membership_country}
			</div>
		</div>

		<div class="grid-x grid-padding-x">
			<div class="cell medium-4">
				{VAR:membership_email}
			</div>
			<div class="cell medium-4">
				{VAR:membership_phone}
			</div>
			<div class="cell medium-4">
				{VAR:membership_birthday}
			</div>
		</div>

		<div class="grid-x grid-padding-x">
			<div class="cell">
				<p class="info">
					Der Mitgliedsbeitrag wird jährlich erhoben, auch im Eintrittsjahr. Der Einzug erfolgt nach Eintritt und anschließend in der Regel im Januar. Für die Höhe des Beitrags kann einer der folgenden Beträge gewählt oder ein höherer Betrag eingetragen werden:
				</p>
			</div>
		</div>

		<div class="form-field form-field--payment" {IF({ISSET:membership_amount_invalid})}error{ENDIF}>
			{IF({ISSET:membership_amount_invalid})}
			<p class="error message">Bitte Betrag wählen oder einen Betrag eingeben, der höher ist als 100,00 &euro;</p>
			{ENDIF}
			<div class="button-group">
				<input id="membership-amount-1" type="radio" name="membership_amount" value="70" {IF("{VAR:membership_amount}" == "70")}checked{ENDIF}>
				<label for="membership-amount-1" class="button">70,&mdash;&nbsp;&euro;</label>

				<input id="membership-amount-2" type="radio" name="membership_amount" value="100" {IF("{VAR:membership_amount}" == "100")}checked{ENDIF}>
				<label for="membership-amount-2" class="button">100,&mdash;&nbsp;&euro;</label>

				<input id="membership-amount-custom" type="radio" name="membership_amount" value="custom" {IF({ISSET:membership_amount_custom})}checked{ENDIF}>
				<label onclick="javascript:document.getElementById('membership-amount-custom-text').focus()" for="membership-amount-custom" class="button">Höherer Betrag</label>

				<input type="text" pattern="^\s*$|^[1-9]\d{2,}" id="membership-amount-custom-text" name="membership_amount_custom" value="{VAR:membership_amount_custom}" placeholder="Höheren Euro-Betrag eingeben">
				<label for="membership-amount-custom-text">,&mdash;&nbsp;&euro;</label>
			</div>
		</div>
	</fieldset>

	<fieldset class="fieldset">
		<legend>Bankverbindung und Beitragseinzug</legend>
		<div class="info stack">
			<p>
				Ich ermächtige den Förderverein Cursillo-Haus St. Jakobus, Kapellenberg 58, 89610 Oberdischingen, Gläubiger-ID DE92ZZZ00000169615, Mandatsreferenz: Eintrittsdatum, Zahlungen von meinem Konto mittels Lastschrift einzuziehen.
			</p>
			<p>
				Zugleich weise ich mein Kreditinstitut an, die vom Förderverein Cursillo-Haus St. Jakobus auf mein Konto gezogenen Lastschriften einzulösen.
			</p>
			<p>
				Hinweis: Ich kann innerhalb von acht Wochen, beginnend mit dem Belastungsdatum, die Erstattung des belasteten Betrages verlangen.
			</p>
			<p>
				Es gelten dabei die mit meinem Kreditinstitut vereinbarten Bedingungen.
			</p>
		</div>

		<div class="grid-x grid-padding-x">
			<div class="cell medium-6">
				<!-- <label>Hiermit ermächtige ich das Cursillo-Haus St. Jakobus bis auf Wiederruf, bla bla ...</label> -->
				{VAR:membership_allow_direct_debit}
			</div>
		</div>

		<div class="grid-x grid-padding-x">
			<div class="cell medium-6">
				{VAR:membership_account_holder}
			</div>
			<div class="cell medium-6">
				{VAR:membership_bank}
			</div>
		</div>

		<div class="grid-x grid-padding-x">
			<div class="cell medium-6">
				{VAR:membership_iban}
			</div>
			<div class="cell medium-6">
				{VAR:membership_bic}
			</div>
		</div>

		<div class="grid-x grid-padding-x">
			<div class="cell medium">
				<div class="info">
					Die mit einem <span>*</span> gekennzeichneten Felder müssen ausgefüllt werden.
				</div>
			</div>
		</div>
	</fieldset>

	<fieldset class="fieldset">
		<legend>Datenschutz</legend>
		<div class="grid-x grid-padding-x">
			<div class="cell medium">
				<div class="form-field form-field--flag required">
					<input type="checkbox" name="data_privacy_statement_accepted" id="data-privacy-statement-accepted" {IF({ISSET:data_privacy_statement_accepted})}checked{ENDIF} required />
					<label for="data-privacy-statement-accepted">Ich habe die <a href="{PAGEURL:58}" target="_blank">Datenschutzerklärung</a> gelesen und erkläre mich hiermit einverstanden.</label>
				</div>
			</div>
		</div>
	</fieldset>

	<div class="action-area">
		<button type="submit" class="button">Absenden</button>
		<a class="" href="{VAR:course_detail_url}">Zurück</a>
	</div>

</form>

