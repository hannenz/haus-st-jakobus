<p>Ein Antrag auf Mitgliedschaft im Förderverein ist eingegangen mit den folgenden Daten:</p>

<h3>1. Persönliche Daten</h3>
<table>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Name/Firma:</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:membership_salutation} {VAR:membership_firstname} {VAR:membership_lastname}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Anschrift:</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">
			{VAR:membership_street_address}<br>
			{VAR:membership_zip} {VAR:membership_city}<br>
			{IF({ISSET:membership_country})}{VAR:membership_country}{ENDIF}
		</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">E-Mail:</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:membership_email}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Telefon:</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:membership_phone}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Mitgliedsbeitrag</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:membership_amount} &euro;</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Geburtsdatum</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:membership_birthday}</td>
	</tr>
</table>

<h3>2. Bankverbindung</h3>
<table>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Kontoinhaber:</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:membership_account_holder}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Kreditinstitut:</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:membership_bank}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">IBAN:</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:membership_iban}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">BIC:</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:membership_bic}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Lastschrifteinzug genehmigt</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:membership_allow_direct_debit}</td>
	</tr>
</table>
