Eine Anmeldung ist eingegangen

Name: 			{VAR:membership_salutation} {VAR:membership_firstname} {VAR:membership_lastname}
Anschrift: 		{VAR:membership_street_address}
				{VAR:membership_zip} {VAR:membership_city}
				{IF({ISSET:membership_country})}{VAR:membership_country}{ENDIF}
E-Mail: 		{VAR:membership_email}
Telefon: 		{VAR:membership_phone}
Geburtsdatum: 	{VAR:membership_birthday}


Bankverbindung

Kontoinhaber: 	{VAR:membership_account_holder}
IBAN: 			{VAR:membership_iban}
BIC: 			{VAR:membership_bic}
Lastschrifteinzug genehmigt: {VAR:membership_allow_direct_debit}

--
