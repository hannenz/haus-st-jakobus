Liebes zukünftiges Mitglied des Fördervereins Cursillo-Haus St. Jakobus,

herzlichen Dank für Ihre Beitrittserklärung.
Wir freuen uns darüber, bearbeiten diese und melden uns danach bei Ihnen.

Herzliche Grüße
David Langer
Vorsitzender
