<p>Liebes zukünftiges Mitglied des Fördervereins Cursillo-Haus St. Jakobus,</p>

<p>
herzlichen Dank für Ihre Beitrittserklärung.<br>
Wir freuen uns darüber, bearbeiten diese und melden uns danach bei Ihnen.
</p>
<p></p>
Herzliche Grüße<br>
David Langer<br>
Vorsitzender
</p>
