Es ist eine Pilgerbettbuchung eingegangen mit folgenden Daten
	
Anrede: 				{VAR:booking_salutation}
Vorname:				{VAR:booking_firstname}
Nachname:				{VAR:booking_lastname}
Anschrift:				{VAR:booking_street_address}
Postleitzahl:			{VAR:booking_zip}
Ort:					{VAR:booking_city}
Land:					{VAR:booking_country}
E-Mail Adresse:			{VAR:booking_email}
Telefon:				{VAR:booking_phone}
Ankunft:				{VAR:booking_arrival}
Abreise:				{VAR:booking_departure}
Erwachsene/Jgdl. ab 13:	{VAR:booking_adults}
Kinder 6 - 12:			{VAR:booking_children}
Kinder 0 - 5:			{VAR:booking_infants}
Unterbringung:			{VAR:booking_accomodation}
Abendessen:				{IF("{VAR:booking_dinner}" == "1")}Ja{ELSE}Nein{ENDIF}
Frühstück:				{IF("{VAR:booking_breakfast}" == "1")}Ja{ELSE}Nein{ENDIF}
Vegetarisches Essen:	{IF("{VAR:booking_vegetarian}" == "1")}Ja{ELSE}Nein{ENDIF}
Bemerkungen:
{VAR:booking_remarks}
