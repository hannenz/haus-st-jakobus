<p>Es ist eine Pilgerbettbuchung eingegangen mit folgenden Daten</p>
<table>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Datum der Anfrage</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{DATEFMT:"{VAR:booking_date}":"%a., %d.%m.%Y %T"}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Anrede</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:booking_salutation}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Vorname</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:booking_firstname}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Nachname</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:booking_lastname}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Anschrift</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:booking_street_address}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Postleitzahl</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:booking_zip}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Ort</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:booking_city}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Land</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:booking_country}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">E-Mail Adresse</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:booking_email}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Telefon</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:booking_phone}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Ankunft</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{DATEFMT:"{VAR:booking_arrival}":"%a., %d.%m.%Y"}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Abreise</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{DATEFMT:"{VAR:booking_departure}":"%a., %d.%m.%Y"}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Anzahl Erwachsene / Jugendliche ab 13</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:booking_adults}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Anzahl Kinder 6&thinsp;&ndash;&thinsp;12</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:booking_children}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Anzahl Kinder 0&thinsp;&ndash;&thinsp;5</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:booking_infants}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Unterbringung</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:booking_accomodation}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Abendessen</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{IF("{VAR:booking_dinner}" == "1")}Ja{ELSE}Nein{ENDIF}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Frühstück</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{IF("{VAR:booking_breakfast}" == "1")}Ja{ELSE}Nein{ENDIF}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Vegetarisches Essen</td>
		<td style="vertical-align: top; padding: 4px 32px 4px 0">{IF("{VAR:booking_vegetarian}" == "1")}Ja{ELSE}Nein{ENDIF}</td>
	</tr>
	<tr style="border-top: 1px solid #e0e0e0;">
		<td style="vertical-align: top; padding: 4px 32px 4px 0">Bemerkungen</td>
		<td style="vertical-align: top" style="vertical-align: top; padding: 4px 32px 4px 0">{VAR:booking_remarks:nl2br}</td>
	</tr>
</table>
