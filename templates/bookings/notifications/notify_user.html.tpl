<p>
	Liebe/r Pilgernde oder Gast,
</p>

<p>
	herzlichen Dank für die Bettenbuchung.
</p>

<p>
	Solange keine andere Antwort kommt, gilt die Buchung als bestätigt.
</p>

<p>
	Buen Camino mit herzlichen Pilgergrüßen<br>
	David Langer<br>
	Hausleitung
</p>
