<form style="scroll-margin-top: 200px" id="registration" action="#registration" method="post" novalidate autocomplete="off">

	{IF({ISSET:saveFailed:VAR})}
		<div class="error message">Bitte prüfen Sie Ihre Eingabe und versuchen Sie es noch einmal!</div>
	{ENDIF}
	{IF({ISSET:data-privacy-statement-not-accepted})}
		<p class="error message message--error">Sie müssen unserer Datenschutzerklärung zustimmen, damit wir Ihre Anmeldung verarbeiten können.</p>
	{ENDIF}

	<fieldset class="fieldset">
		<legend>Persönliche Angaben</legend>

		<div class="grid-x grid-padding-x">
			<div class="cell">
				<div class="info">
					Die mit einem <span>*</span> gekennzeichneten Felder müssen ausgefüllt werden.
				</div>
			</div>
		</div>

		<div class="grid-x grid-padding-x">
			<div class="cell medium-3">
				{VAR:booking_salutation}
			</div>
			<div class="cell medium-4">
				{VAR:booking_firstname}
			</div>
			<div class="cell medium-5">
				{VAR:booking_lastname}
			</div>
		</div>

		<div class="grid-x grid-padding-x">
			<div class="cell">
				{VAR:booking_street_address}
			</div>
		</div>

		<div class="grid-x grid-padding-x">
			<div class="cell medium-2">
				{VAR:booking_zip}
			</div>
			<div class="cell medium-5">
				{VAR:booking_city}
			</div>
			<div class="cell medium-5">
				{VAR:booking_country}
			</div>
		</div>

		<div class="grid-x grid-padding-x">
			<div class="cell medium-6">
				{VAR:booking_email}
			</div>
			<div class="cell medium-6">
				{VAR:booking_phone}
			</div>
		</div>
	</fieldset>

	<fieldset class="fieldset">
		<legend>Übernachtung</legend>

		<div class="grid-x grid-padding-x">
			<div class="cell medium-6">
				{VAR:booking_arrival}
			</div>
			<div class="cell medium-6">
				{VAR:booking_departure}
			</div>
		</div>

		<div class="grid-x grid-padding-x">
			<div class="cell medium-4">
				{VAR:booking_adults}
			</div>
			<div class="cell medium-4">
				{VAR:booking_children}
			</div>
			<div class="cell medium-4">
				{VAR:booking_infants}
			</div>
		</div>

		<div class="grid-x grid-padding-x">
			<div class="cell medium-12">
				<div class="form-field--radio">
					<div class="radio-group stack">
						<p><label>Ich wünsche</label></p>
						
						<div class="radio-wrapper">
							<input 
								type="radio"
								name="booking_accomodation"
								id="registration-accomodation--double-room"
								value="Mehrfachzimmer"
								{IF('{VAR:booking_accomodation_value}' == 'Mehrfachzimmer')} checked {ENDIF}
							/>
							<label for="registration-accomodation--double-room">
								Übernachtung im Doppel- oder Dreibettzimmer
								<details>
									<summary><em>Hinweis</em></summary>
									<span>Einzelne Pilgernde/Gäste, die hier Doppelzimmer wünschen, bekommen vor Ort den Doppelzimmerpreis, wenn das zweite Bett im Zimmer von einer zweiten Person genutzt wurde. Ansonsten gilt der Einzelzimmerpreis.</span>
								</details>
							</label>
						</div>
						<div class="radio-wrapper">
							<input
								type="radio"
								name="booking_accomodation"
								id="registration-accomodation--single-room"
								value="Einzelzimmer"
								{IF('{VAR:booking_accomodation_value}' == 'Einzelzimmer')}checked{ENDIF}
							/>
							<label for=registration-accomodation--single-room />Übernachtung im Einzelzimmer</label>
						</div>
						<div class="radio-wrapper">
							<input
								type="radio"
								name="booking_accomodation"
								id="registration-accomodation--single-room-preferred"
								value="Einzelzimmer-bevorzugt" 
								{IF('{VAR:booking_accomodation_value}' == 'Einzelzimmer-bevorzugt')}checked{ENDIF}
							/>
							<label for=registration-accomodation--single-room-preferred>Übernachtung im Einzelzimmer bevorzugt, wenn kein Einzelzimmer frei ist, bin ich auch bereit im Doppelzimmer zu übernachten</label>
						</div>
						<div class="radio-wrapper">
							<input
								type="radio"
								name="booking_accomodation"
								id="registration-accomodation--camping"
								value="Zeltplatz-Schlafplatz-im-Freien"
								{IF('{VAR:booking_accomodation_value}' == 'Zeltplatz-Schlafplatz-im-Freien')}checked{ENDIF}
							/>
							<label for=registration-accomodation--none>Zeltplatz/Schlafplatz im Freien</label>
						</div>
					</div>
				</div>
				<div class="stack stack--flags">
					<div>
						<div class="form-field form-field--flag">
							{VAR:booking_dinner}
						</div>
					</div>
					<div>
						<div class="form-field form-field--flag">
							{VAR:booking_breakfast}
						</div>
					</div>
					<div>
						<div class="form-field form-field--flag">
							{VAR:booking_vegetarian}
						</div>
					</div>
				<div>
			<div>
		</div>
	</fieldset>

	<fieldset class="fieldset">
		<legend>Anmerkungen</legend>
		<div class="form-field form-field--textarea">
			{VAR:booking_remarks}
		</div>
	</fieldset>

	<fieldset class="fieldset">
		<legend>Datenschutz</legend>
		<div class="form-field form-field--flag">
			<input type="checkbox" name="data-privacy-statement-accepted" id="data-privacy-statement-accepted" {IF("{VAR:data-privacy-statement-accepted}" == "1")}checked{ENDIF} />
			<label for="data-privacy-statement-accepted">Ich habe die <a href="{PAGEURL:57}" target="_blank">Datenschutzerklärung</a> gelesen und erkläre mich hiermit einverstanden.</label>
		</div>
	</fieldset>

	<div class="action-area">
		<button type="submit" class="button">Absenden</button>
		<a class="" href="{VAR:course_detail_url}">Zurück</a>
	</div>

</form>
