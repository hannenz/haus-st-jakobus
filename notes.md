- E-Mail Konten
- Umstellung PayPal Spende (s.u.) Betrag aus Formular raus.
- GPX-Strecken
- Upgrade: 12.8.
 
 

2024-05-05

# Umstellung auf Paypal

Bisheriger Flow:

## Mitgliedschaft:

User füllt Formular aus mit Betrag (70, 100, custom > 100) und Bankverbindungsdaten (IBAN, BIC)
User bekommt E-Mail 

```
Liebes zukünftiges Mitglied des Fördervereins Cursillo-Haus St. Jakobus,

herzlichen Dank für Ihre Beitrittserklärung.
Wir freuen uns darüber, bearbeiten diese und melden uns danach bei Ihnen. 
```

Admin wird benachrichtigt

Alle Daten des Formuilars.

Wie sieht die "interne Bearbeitung" aus?
Wird der User dann manuell noch einmal benachrichtigt??




## Pilgerpass

User füllt Formular aus mit Wunschbetrag (5, 10, 20, custom) und Bezahlart (Giropay, Überweisung, Bar)

### Giropay
Screen Bestellung ist eingegangen >> Button ("Jetzt mit Giropay bezahlen") => Dev. kaputt


### Überweisung
Screen Bestellung ist eingegangen >> Banverbindung und Hinweis zum überweisen + Button "Diese Seite drucken"

### Bargeld
Screen Bestellung ist eingegangen >> Banverbindung und Hinweis zum überweisen + Button "Diese Seite drucken"
```
Bitte senden Sie den Betrag von € per Post an diese Adresse

Cursillo-Haus St. Jakobus
Kapellenberg 58-60
D–89610 Oberdischingen

Bitte vermerken Sie in Ihrem Schreiben die Bestellnr. 1389 
```

